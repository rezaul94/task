import com.opencsv.CSVWriter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

public class Scrapper {
    String url = "https://finance.yahoo.com/quote/%5EVIX/history?p=%5EVIX";

    public void readWebPage() throws Exception{
        Document doc = Jsoup.connect(url).get();

        List<String[]> allLines = new ArrayList<String[]>();

        Elements headerElements = doc.select("th");
        String [] header = new String[headerElements.size()];
        for (int i = 0; i < headerElements.size(); i++) {
            header[i] = headerElements.get(i).text();
        }
        allLines.add(header);


        boolean hasData = false;
        Elements tableBodyElements = doc.select("tbody");
        for (Element tableBodyElement : tableBodyElements) {
            Elements rowElements = tableBodyElement.select("tr");
            for (Element rowElement : rowElements) {
                Elements dataElements = rowElement.select("td");
                String [] row = new String[dataElements.size()];
                for (int i = 0; i < dataElements.size(); i++) {
                    row[i] = dataElements.get(i).text();
                    hasData = true;
                }
                allLines.add(row);
            }
        }

        if (!hasData)
            throw new RuntimeException("No Data Found.");

        File file = new File("CBOE.csv");
        FileWriter fileWriter = new FileWriter(file);
        CSVWriter csvWriter = new CSVWriter(fileWriter);
        csvWriter.writeAll(allLines);
        csvWriter.close();
    }


}
